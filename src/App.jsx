//创建“外壳”组件App
import React,{Component} from 'react'
import Hello from './components/Hello'


//创建并暴露App组件
export default class App extends Component{
	constructor(props) {
		super(props);
		this.state = {
			todos:[],
			nowItem :'',
			showtype:'all'
		};
	  }
	  inputAdd = (e)=>{
		this.setState({
			nowItem:e.target.value,
		})
	  }
	  handlClick = () =>{
		if(this.state.nowItem==='') {
			alert('错误！输入为空❌')
			return
		} 
		const addObj = {id:this.state.todos.length,value:this.state.nowItem,done:false}
		this.setState({
			todos:this.state.todos.concat(addObj)
		})
		this.setState({
			nowItem:''
		})
	}
	handleCheck = (e) =>{
		const changeItem = this.state.todos;
		changeItem[e.id].done = true;
		this.setState({
			todos:changeItem
		})
	}
	handlShowtype = (num) =>{
		if(num===1){
			this.setState({
				showtype:'all'
			})
		}else if(num===2){
			this.setState({
				showtype:'completed'
			})
		}else{
			this.setState({
				showtype:'undo'
			})
		}
	}

	render(){
		const todolist = this.state.todos;
		const List =todolist.map((item)=>{
			if(this.state.showtype==='completed'){
				if(item.done!==true){
					return
				}
			}else if(this.state.showtype==='undo'){
				if(item.done!==false){
					return
				}
			}
			return (
				<li key={item.id}>
					<span>{item.value}</span>
					&nbsp;
					<input type='checkbox' onChange={()=>this.handleCheck(item)}></input>
				</li>
			)
		})
		return (
			<div>
				<Hello/>
				<div className="todo-container">
					<input onChange={this.inputAdd} type="text" placeholder="请输入你的任务名称，按回车键确认"/>
					<button onClick={this.handlClick}>add</button>
					
					<div className="showList">
						<ol className='todoMain'>
							{List}
						</ol>
					</div>

					<div className='choose'>
						选择：
						&nbsp;
						<button onClick={()=>this.handlShowtype(1)}>所有</button>
						<button onClick={()=>this.handlShowtype(2)}> 已完成</button>
						<button onClick={()=>this.handlShowtype(3)}>未完成</button>
					</div>
				</div>
			</div>
		)
	}
}


